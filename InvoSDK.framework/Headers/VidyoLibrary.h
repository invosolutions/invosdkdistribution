//
//  VidyoLibrary.h
//  VidyoSwiftSample
//
//  Created by ApiSamples on 07/07/17.
//  Copyright © 2017 ApiSamples. All rights reserved.
//

#ifndef VidyoLibrary_h
#define VidyoLibrary_h


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@protocol VideoViewDelegate <NSObject>

@required
- (void) callEndedCallback;
- (void) callStartedCallback;
- (void) groupMessageCallback: (NSString*) message;
- (void) newParticipantCallback;

@end

@interface VidyoLibrary : NSObject

//@property (nonatomic, strong) id<VideoViewDelegate> delegate;

-(void) setDelegate: (id<VideoViewDelegate>) newDelegate;

-(void) ConnectToRoom:(NSString*) portalUrl :(NSString*) key :(NSString*)name;
-(void) setOrientation:(int)orientation;
-(void) orientationDidChange;
-(void) resize:(int)height :(int)width :(int)x :(int)y;
-(void) statusBarDidChangeFrame;
-(void) applicationWillTerminate:(UIApplication *)application;
-(void) applicationWillResignActive:(UIApplication *)application;
-(void) applicationDidBecomeActive:(UIApplication *)application;
-(BOOL) vidyoClientStarted;
-(BOOL) vidyoInRoom;
-(void) InitializeVidyo: (UIView*) vidyoRender;
-(void) LeaveRoom;

-(void) SetCamera:(int)cameraNum;

-(void) UnmuteCamera;
-(void) MuteCamera;

-(void) UnmuteMicrophone;
-(void) MuteMicrophone;

-(void) UnmuteSpeaker;
-(void) MuteSpeaker;

-(void) HidePreview;
-(void) ShowPreview;

-(void) sendGroupMessage:(NSString *)message;

@end


#endif /* VidyoLibrary_h */
