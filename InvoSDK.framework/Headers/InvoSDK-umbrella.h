#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "InvoVideoBankingiOS-Bridging-Header.h"
#import "VidyoClient.h"
#import "VidyoClientConstants.h"
#import "VidyoClientMessages.h"
#import "VidyoClientParameters.h"
#import "VidyoClientPrivate.h"
#import "VidyoTypes.h"
#import "VidyoLibrary.h"

FOUNDATION_EXPORT double InvoSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char InvoSDKVersionString[];

