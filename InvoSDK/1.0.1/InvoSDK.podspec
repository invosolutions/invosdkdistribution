#
# Be sure to run `pod lib lint InvoSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'InvoSDK'
    s.version          = '1.0.1'
    s.summary          = 'Framework for embeding Invo Solutions Video Banking Solution in iOS application'
  
  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  
    s.description      = <<-DESC
  Framework for embeding Invo Solutions Video Banking Solution in iOS application
                         DESC
  
    s.homepage         = 'https://invosolutions.com'
    # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    # s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Invo Solutions' => 'support@invosolutions.com' }
    s.source           = { :git => 'https://invosolutions@bitbucket.org/invosolutions/invosdkdistribution.git', :tag => s.version.to_s }
    # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
    s.ios.deployment_target = '11.0'
    s.source_files = 'InvoSDK.framework/Headers/*.h'
    s.public_header_files = 'InvoSDK.framework/Headers/*.h'
    s.frameworks = 'CFNetwork', 'Security', 'SystemConfiguration'
    s.library = 'c++'
    # s.ios.vendored_library = 'Libs/VidyoClientLibrary/lib/VidyoClientLib.a'
    
    s.info_plist = {
      'UIRequiredDeviceCapabilities' => ['armv7'],
      'NSCameraUsageDescription' => 'requires camera to provide video streaming with banking agents',
      'NSLocationWhenInUseUsageDescription' => 'uses Location to provide an extra layer of identificaiton',
      'NSMicrophoneUsageDescription' => 'requires microphone to provide audio streaming with banking agents',
      'NSPhotoLibraryAddUsageDescription' => 'App saves screenshots to photo library',
      'UIBackgroundModes' => ['audio']
    }
    
    s.user_target_xcconfig = { 'ENABLE_BITCODE' => 'NO' }
    s.pod_target_xcconfig = { 'ENABLE_BITCODE' => 'NO' }
    
    s.resources = 'InvoSDK.framework/*.png'
  
    s.dependency 'PusherSwift', '~> 7.2'
    s.dependency 'PushNotifications'
    s.dependency 'Alamofire', '~> 4.7'
    s.dependency 'SwiftyJSON', '~> 4.0'
    s.dependency 'Material', '~> 3.1.8'
    s.dependency 'SwiftIcons', '~> 2.3.2'
    s.dependency 'Cosmos', '~> 19.0'
    s.dependency 'Floaty', '~> 4.2.0'
  end
  